Windows10でHerokuクライアントのDockerイメージの利用方法
- 前提
    - Herokuアカウント登録済み
        * https://jp.heroku.com/home
    
    - Windows10にDocker Toolboxのインストール済み
        * https://qiita.com/maemori/items/52b1639fba4b1e68fccd

        - Docker Client
        - Docker Machine
        - Docker Compose (Mac only)
        - Docker Kitematic
        - VirtualBox

- 手順
    - Windowsの場合、Docker Quickstart Terminalを起動する

    - HerokuクライアントのDockerイメージをローカルにインストールする
        ```
        docker pull dickeyxxx/heroku-cli
        ```
    -  Herokuクライアントからログイン
        ```
        docker run -it dickeyxxx/heroku-cli /bin/bash
        heroku login
        Email: yourname@yourdomain.com
        Password:　yourpassword
        Logged in as yourname@yourdomain.com
        root@xxxxxx:/#
        ```
        上記の最後のラインにroot@xxxxxx:/#が表示されたら

- 参考情報
    - HerokuクライアントのDockerイメージ 
    * https://hub.docker.com/r/dickeyxxx/heroku-cli/

- 
Company: 株式会社SUGo https://www.su-go.co.jp